import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:iqoute/custom/theme/appTheme.dart';
import 'package:iqoute/custom/widgets/loading.dart';
import 'package:iqoute/custom/widgets/qoute_card.dart';
import 'package:iqoute/custom/widgets/update.dart';
import 'package:iqoute/models/qoute.dart';

class MyQoutes extends StatefulWidget {
  const MyQoutes({Key? key}) : super(key: key);

  @override
  State<MyQoutes> createState() => _MyQoutesState();
}

class _MyQoutesState extends State<MyQoutes> {
  final Stream<QuerySnapshot> _myQouteStream = FirebaseFirestore.instance
      .collection(FirebaseAuth.instance.currentUser!.uid)
      .snapshots();
  CollectionReference qouteCollections = FirebaseFirestore.instance
      .collection(FirebaseAuth.instance.currentUser!.uid);

  //TODO:Take care commet

  // late SharedPreferences prefs;
  // @override
  // void initState() {
  //   super.initState();
  //   init();
  // }

  // Future init() async {
  //   prefs = await SharedPreferences.getInstance();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
        stream: _myQouteStream,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Loading();
          }
          if (snapshot.hasError) {
            return const Center(child: Text('Something went wrong'));
          }
          return ListView(
            children: snapshot.data!.docs.map(
              (snapshotDoc) {
                Map<String, dynamic> doc =
                    snapshotDoc.data()! as Map<String, dynamic>;
                final Qoute qoute = Qoute(
                    by: doc['by'], from: doc['from'], qoute: doc['qoute']);

                return MyQouteCard(
                  qoute: qoute,
                  onUpdate: () {
                    showModalBottomSheet(
                      backgroundColor: AppTheme.mainColor,
                      elevation: 30,
                      context: context,
                      builder: (context) {
                        return Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 20.0,
                            horizontal: 60.0,
                          ),
                          child: EditQouteForm(
                            by: doc['by'],
                            from: doc['from'],
                            qoute: doc['qoute'],
                            doc: doc,
                          ),
                        );
                      },
                    );
                  },
                  onDelete: () {
                    qouteCollections.doc().delete();
                  },
                );
              },
            ).toList(),
          );
          // ListView.builder(
          //   itemCount: snapshot.data!.docs.length,
          //   itemBuilder: (context, index) {
          //     List<MyQouteCard> myQoutes = snapshot.data!.docs.map(
          //       (snapshotDoc) {
          //         Map<String, dynamic> doc =
          //             snapshotDoc.data()! as Map<String, dynamic>;
          //         final Qoute qoute = Qoute(
          //             by: doc['by'], from: doc['from'], qoute: doc['qoute']);

          //         return MyQouteCard(
          //           qoute: qoute,
          //           onUpdate: () {},
          //           onDelete: () {},
          //         );
          //       },
          //     ).toList();

          //     return myQoutes.isNotEmpty
          //         ? myQoutes[index]
          //         : Center(
          //             child: Text(
          //               'Not yet added qoute\nClick Add Qoute',
          //               style: appTheme.textStyle(size: 14),
          //             ),
          //           );
          //   },
          // );
        },
      ),
    );
  }
}
