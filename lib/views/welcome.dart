// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:iqoute/custom/theme/appTheme.dart';
import 'package:iqoute/custom/widgets/add_qoute_form.dart';
import 'package:iqoute/custom/widgets/button.dart';
import 'package:iqoute/custom/widgets/loading.dart';
import 'package:iqoute/custom/widgets/richtext.dart';
import 'package:iqoute/services/auth.dart';
import 'package:iqoute/views/myqoutes.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Welcome extends StatefulWidget {
  const Welcome({Key? key}) : super(key: key);

  @override
  State<Welcome> createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  bool loading = false;
  final AuthService _auth = AuthService();
  String? _uid;

  late SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    init();
  }

  Future init() async {
    prefs = await SharedPreferences.getInstance();
    String? uid = prefs.getString('userID');
    if (uid == null) return;
    setState(() {
      _uid = uid;
    });
  }

  @override
  Widget build(BuildContext context) {
    void _showAddQoute() {
      showModalBottomSheet(
        backgroundColor: AppTheme.mainColor,
        elevation: 30,
        context: context,
        builder: (context) {
          return Container(
            padding: const EdgeInsets.symmetric(
              vertical: 20.0,
              horizontal: 60.0,
            ),
            child: const AddQouteForm(),
          );
        },
      );
    }

    AppTheme appTheme = AppTheme();
    return loading
        ? const Loading()
        : Scaffold(
            appBar: AppBar(
              elevation: 0,
              title: Text(
                'Welcome to iQuote',
                style:
                    appTheme.textStyle(fontWeight: FontWeight.bold, size: 16),
              ),
            ),
            floatingActionButtonLocation: _uid == null
                ? FloatingActionButtonLocation.centerFloat
                : FloatingActionButtonLocation.endFloat,
            floatingActionButton: _uid == null
                ? TextButton.icon(
                    style: ButtonStyle(
                      splashFactory: NoSplash.splashFactory,
                      overlayColor:
                          MaterialStateProperty.all(Colors.transparent),
                    ),
                    onPressed: () async {
                      dynamic result = await _auth.signInAnon();

                      if (result == null) {
                        setState(() => loading = false);
                      } else if (result != null) {
                        prefs.setString('userID', result.uid);
                        prefs.setInt('uniqueID', 1);
                        setState(
                          () => _uid = prefs.getString('userID'),
                        );
                      }
                    },
                    label: const Text('START'),
                    icon: const Icon(Icons.arrow_forward),
                  )
                : buttton(
                    title: 'Add Qoute',
                    onClicked: () => _showAddQoute(),
                  ),
            body: _uid == null
                ? Card(
                    color: const Color.fromARGB(255, 14, 1, 17),
                    margin: const EdgeInsets.symmetric(
                        vertical: 60, horizontal: 40),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListView(
                        children: [
                          rictText(),
                        ],
                      ),
                    ),
                  )
                : const MyQoutes(),
          );
  }
}
