// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  static Color mainColor = const Color.fromARGB(255, 29, 3, 34);
  Color cardColor = const Color.fromARGB(255, 83, 19, 40);
  Color dark = const Color.fromARGB(255, 28, 26, 26);
  Color ligth = const Color.fromARGB(255, 199, 204, 206);
  Color blueLight = const Color.fromARGB(255, 4, 45, 78);
  static Color cardColor1 = const Color.fromARGB(255, 83, 19, 40);
  static Color dark1 = const Color.fromARGB(255, 28, 26, 26);
  static Color ligth1 = const Color.fromARGB(255, 199, 204, 206);
  static Color blueLight1 = const Color.fromARGB(255, 4, 45, 78);

  ThemeData themeData = ThemeData(
    scaffoldBackgroundColor: mainColor,
    appBarTheme: const AppBarTheme(
      centerTitle: true,
      color: Colors.transparent,
    ),
  );
  TextStyle textStyle({
    double size = 11,
    Color color = const Color.fromARGB(255, 199, 204, 206),
    FontWeight fontWeight = FontWeight.w500,
  }) =>
      GoogleFonts.mcLaren(
        fontSize: size,
        color: color,
        fontWeight: fontWeight,
      );

  static TextStyle textStyle1({
    double size = 11,
    Color color = const Color.fromARGB(255, 199, 204, 206),
    FontWeight fontWeight = FontWeight.w500,
  }) =>
      GoogleFonts.mcLaren(
        fontSize: size,
        color: color,
        fontWeight: fontWeight,
      );
}
