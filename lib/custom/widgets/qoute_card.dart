import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:iqoute/custom/theme/appTheme.dart';
import 'package:iqoute/models/qoute.dart';

class MyQouteCard extends StatelessWidget {
  final Qoute qoute;
  final Function() onUpdate;
  final Function() onDelete;
  const MyQouteCard({
    Key? key,
    required this.qoute,
    required this.onUpdate,
    required this.onDelete,
  }) : super(key: key);
  CollectionReference qouteCollections() => FirebaseFirestore.instance
      .collection(FirebaseAuth.instance.currentUser!.uid);

  @override
  Widget build(BuildContext context) {
    AppTheme appTheme = AppTheme();
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Card(
        margin: const EdgeInsets.fromLTRB(10, 6, 10, 0),
        child: ListTile(
          tileColor: appTheme.cardColor,
          onTap: () {},
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'by: ${qoute.by}',
                    style: appTheme.textStyle(),
                  ),
                  Row(
                    children: [
                      InkWell(
                        onDoubleTap: () {},
                        onTap: () {},
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        focusColor: Colors.transparent,
                        child: const Icon(
                          Icons.edit,
                          color: Colors.blue,
                        ),
                      ),
                      const SizedBox(width: 10),
                      InkWell(
                        onTap: () {},
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        focusColor: Colors.transparent,
                        child: const Icon(
                          Icons.delete_forever,
                          color: Colors.redAccent,
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Divider(
                color: appTheme.dark,
                height: 12,
              ),
              Text(
                ' " ${qoute.qoute} " ',
                style: appTheme.textStyle().copyWith(
                      color: Colors.white,
                      fontSize: 16,
                    ),
              ),
              Divider(
                color: appTheme.dark,
                height: 12,
              ),
              Text(
                'from: ${qoute.from}',
                style: appTheme.textStyle(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
