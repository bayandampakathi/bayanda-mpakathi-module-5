// ignore_for_file: file_names

import 'package:flutter/material.dart';

const inputDec = InputDecoration(
  enabledBorder: OutlineInputBorder(
    gapPadding: 10,
    borderSide: BorderSide(
      color: Colors.pink,
      style: BorderStyle.solid,
    ),
  ),
  border: OutlineInputBorder(
    gapPadding: 10,
    borderSide: BorderSide(
      color: Colors.pink,
      style: BorderStyle.solid,
    ),
  ),
  filled: true,
  fillColor: Color.fromARGB(255, 29, 3, 34),
);
