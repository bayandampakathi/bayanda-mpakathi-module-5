import 'package:flutter/material.dart';
import 'package:iqoute/custom/theme/appTheme.dart';

Widget rictText() {
  AppTheme appTheme = AppTheme();
  return RichText(
    overflow: TextOverflow.clip,
    textScaleFactor: 1.5,
    textAlign: TextAlign.center,
    text: TextSpan(
      style: appTheme.textStyle(size: 13),
      children: [
        const TextSpan(
          text: 'Do you how ',
        ),
        TextSpan(
          text: 'POWERFUL ',
          style: appTheme.textStyle(
            size: 18,
            fontWeight: FontWeight.w800,
            color: Colors.yellowAccent,
          ),
        ),
        const TextSpan(
          text: 'your ',
        ),
        TextSpan(
            text: 'words ',
            style: appTheme.textStyle(
              fontWeight: FontWeight.w900,
              color: const Color.fromARGB(255, 26, 82, 28),
              size: 14,
            )),
        const TextSpan(
          text: 'are!!??',
        ),
        TextSpan(
          children: [
            const TextSpan(
              text: ', It is scientific proven that ',
            ),
            TextSpan(
              style: appTheme.textStyle(size: 13, fontWeight: FontWeight.bold),
              text: '95% ',
            ),
            const TextSpan(
              text: 'of words you use when you speak to someone can cause ',
            ),
            TextSpan(
              style: appTheme.textStyle(
                size: 15,
                fontWeight: FontWeight.bold,
                color: const Color.fromARGB(255, 231, 19, 4),
              ),
              text: 'emotional damage ...\n',
            ),
            TextSpan(
              style: appTheme.textStyle(
                size: 15,
                color: Colors.green,
              ),
              text:
                  '\nBUT IN THIS SPACE WE ARE HERE TO DELIVER HEALING WORDS\n',
            ),
            TextSpan(
              style: appTheme.textStyle(
                size: 12,
                color: const Color.fromARGB(255, 213, 213, 19),
              ),
              text: '\nWe qoute from those heals us\n',
            ),
            TextSpan(
              style: appTheme.textStyle(
                size: 12,
                color: const Color.fromARGB(255, 213, 213, 19),
              ),
              text: 'We qoute from those gives hope\n',
            ),
            TextSpan(
              style: appTheme.textStyle(
                size: 12,
                color: const Color.fromARGB(255, 213, 213, 19),
              ),
              text: 'We qoute from those gives power\n',
            ),
            TextSpan(
              style: appTheme.textStyle(
                size: 12,
                color: const Color.fromARGB(255, 213, 213, 19),
              ),
              text: 'We qoute from those gives strength\n',
            ),
          ],
        ),
      ],
    ),
  );
}
