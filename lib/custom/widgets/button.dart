import 'package:flutter/material.dart';
import 'package:iqoute/custom/theme/appTheme.dart';

Widget buttton({
  required String title,
  required Function() onClicked,
}) =>
    MaterialButton(
      color: AppTheme.blueLight1,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18),
      ),
      onPressed: onClicked,
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: Text(
          title,
          style: AppTheme.textStyle1(),
        ),
      ),
    );
