import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:iqoute/custom/theme/appTheme.dart';
import 'package:iqoute/custom/widgets/button.dart';
import 'package:iqoute/custom/widgets/inpuDec.dart';

class EditQouteForm extends StatefulWidget {
  final String by;
  final String from;
  final String qoute;
  final Map doc;
  const EditQouteForm({
    Key? key,
    required this.by,
    required this.from,
    required this.qoute,
    required this.doc,
  }) : super(key: key);

  @override
  State<EditQouteForm> createState() => _EditQouteFormState();
}

class _EditQouteFormState extends State<EditQouteForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController byTxtCtr = TextEditingController();
  TextEditingController fromTxtCtr = TextEditingController();
  TextEditingController qouteTxtCtr = TextEditingController();
  CollectionReference qouteCollections = FirebaseFirestore.instance
      .collection(FirebaseAuth.instance.currentUser!.uid);

  @override
  Widget build(BuildContext context) {
    byTxtCtr.text = widget.by;
    fromTxtCtr.text = widget.from;
    qouteTxtCtr.text = widget.qoute;
    AppTheme appTheme = AppTheme();
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Text(
            'Edit Qoute',
            style: appTheme.textStyle(
              size: 18,
              color: appTheme.ligth,
            ),
          ),
          const SizedBox(height: 20),
          Column(
            children: [
              TextFormField(
                controller: byTxtCtr,
                style: appTheme.textStyle(
                  size: 18,
                  color: appTheme.ligth,
                ),
                decoration: inputDec.copyWith(
                  label: Text(
                    'by',
                    style: appTheme.textStyle(
                      size: 18,
                      color: appTheme.ligth,
                    ),
                  ),
                ),
                validator: (val) =>
                    val!.isEmpty ? 'please enter the qoute source' : null,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: qouteTxtCtr,
                style: appTheme.textStyle(
                  size: 18,
                  color: appTheme.ligth,
                ),
                decoration: inputDec.copyWith(
                  label: Text(
                    'qoute',
                    style: appTheme.textStyle(
                      size: 18,
                      color: appTheme.ligth,
                    ),
                  ),
                ),
                validator: (val) =>
                    val!.isEmpty ? 'please enter the qoute' : null,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: fromTxtCtr,
                style: appTheme.textStyle(
                  size: 18,
                  color: appTheme.ligth,
                ),
                decoration: inputDec.copyWith(
                  label: Text(
                    'from',
                    style: appTheme.textStyle(
                      size: 18,
                      color: appTheme.ligth,
                    ),
                  ),
                ),
                validator: (val) =>
                    val!.isEmpty ? 'please enter the qoutee' : null,
              ),
            ],
          ),
          const SizedBox(height: 20),
          buttton(
            title: 'Edit Qoute',
            onClicked: () {
              Map<String, dynamic> updated = {
                'by': byTxtCtr.text,
                'from': fromTxtCtr.text,
                'qoute': qouteTxtCtr.text,
              };
              widget.doc.update('by', updated['by']);
              widget.doc.update('from', updated['from']);
              widget.doc.update('qoute', updated['qoute']);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
