import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:iqoute/custom/theme/appTheme.dart';

class Loading extends StatefulWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  State<Loading> createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppTheme.mainColor,
      child: const Center(
        child: SpinKitPumpingHeart(
          color: Colors.pinkAccent,
        ),
      ),
    );
  }
}
