import 'package:flutter/material.dart';
import 'package:iqoute/custom/theme/appTheme.dart';
import 'package:iqoute/custom/widgets/button.dart';
import 'package:iqoute/custom/widgets/inpuDec.dart';
import 'package:iqoute/models/qoute.dart';
import 'package:iqoute/services/database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddQouteForm extends StatefulWidget {
  const AddQouteForm({Key? key}) : super(key: key);

  @override
  State<AddQouteForm> createState() => _AddQouteFormState();
}

class _AddQouteFormState extends State<AddQouteForm> {
  final _formKey = GlobalKey<FormState>();
  String currentBy = '';
  String currentQoute = '';
  String currentFrom = '';

  @override
  Widget build(BuildContext context) {
    AppTheme appTheme = AppTheme();
    return Form(
      key: _formKey,
      child: ListView(
        children: [
          Text(
            'Add Qoute',
            style: appTheme.textStyle(
              size: 18,
              color: appTheme.ligth,
            ),
          ),
          const SizedBox(height: 20),
          Column(
            children: [
              TextFormField(
                style: appTheme.textStyle(
                  size: 18,
                  color: appTheme.ligth,
                ),
                decoration: inputDec.copyWith(
                  label: Text(
                    'by',
                    style: appTheme.textStyle(
                      size: 18,
                      color: appTheme.ligth,
                    ),
                  ),
                ),
                validator: (val) =>
                    val!.isEmpty ? 'please enter the qouter' : null,
                onChanged: (val) {
                  setState(() {
                    currentBy = val;
                  });
                },
              ),
              const SizedBox(height: 20),
              TextFormField(
                style: appTheme.textStyle(
                  size: 18,
                  color: appTheme.ligth,
                ),
                decoration: inputDec.copyWith(
                  label: Text(
                    'qoute',
                    style: appTheme.textStyle(
                      size: 18,
                      color: appTheme.ligth,
                    ),
                  ),
                ),
                validator: (val) =>
                    val!.isEmpty ? 'please enter the qoute' : null,
                onChanged: (val) {
                  setState(() {
                    currentQoute = val;
                  });
                },
              ),
              const SizedBox(height: 20),
              TextFormField(
                style: appTheme.textStyle(
                  size: 18,
                  color: appTheme.ligth,
                ),
                decoration: inputDec.copyWith(
                  label: Text(
                    'from',
                    style: appTheme.textStyle(
                      size: 18,
                      color: appTheme.ligth,
                    ),
                  ),
                ),
                validator: (val) =>
                    val!.isEmpty ? 'please enter the qoutee' : null,
                onChanged: (val) {
                  setState(() {
                    currentFrom = val;
                  });
                },
              ),
            ],
          ),
          const SizedBox(height: 20),
          buttton(
            title: 'Add Qoute',
            onClicked: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              final id = prefs.getInt('uniqueID');
              if (_formKey.currentState!.validate()) {
                Qoute qoute = Qoute(
                  by: currentBy,
                  from: currentFrom,
                  qoute: currentQoute,
                  time: DateTime.now().millisecondsSinceEpoch,
                );
                final String uniqueID = id.toString();
                final dbServices = DatabaseServices(prefs: prefs);

                dbServices
                    .addQoute(
                      qoute: qoute,
                      uniqueID: uniqueID,
                    )
                    .then((value) {})
                    .catchError((error) {});

                prefs.setInt('uniqueID', id! + 1);

                // ignore: use_build_context_synchronously
                Navigator.pop(context);
              }
            },
          ),
        ],
      ),
    );
  }
}
