import 'package:firebase_auth/firebase_auth.dart';
import 'package:iqoute/models/user.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  UserModel? _userFromFb(User? user) {
    return user != null ? UserModel(uid: user.uid) : null;
  }

  Future signInAnon() async {
    try {
      UserCredential result = await _auth.signInAnonymously();
      User? user = result.user;
      return _userFromFb(user);
    } catch (e) {
      return null;
    }
  }
}
