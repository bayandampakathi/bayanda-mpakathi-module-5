import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:iqoute/custom/widgets/qoute_card.dart';
import 'package:iqoute/models/qoute.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DatabaseServices {
  //Referent to persitence local data which is userID and uniqueID
  SharedPreferences prefs;
  //collection references
  DatabaseServices({required this.prefs});
  String? get uid {
    return prefs.get('userID').toString();
  }

  //Reference to the Collection
  CollectionReference qouteCollections() =>
      FirebaseFirestore.instance.collection(uid!);

  //Qoutes Stream
  Stream<QuerySnapshot> myQouteStream() => FirebaseFirestore.instance
      .collection(FirebaseAuth.instance.currentUser!.uid)
      .snapshots();

  //Create qoute to DB
  Future addQoute({required Qoute qoute, required String uniqueID}) async {
    final myQoute = qoute.toDatabase(qoutee: qoute);
    return await qouteCollections().doc(uniqueID.toString()).set(myQoute);
  }

  //Read qoutes from db
  List<MyQouteCard> readAllQoutes({
    required AsyncSnapshot<QuerySnapshot<Object?>> snapshot,
    required String index,
  }) {
    List<MyQouteCard> myqoutes = snapshot.data!.docs.map(
      (snapshotDoc) {
        Map<String, dynamic> doc = snapshotDoc.data()! as Map<String, dynamic>;
        final Qoute qoute =
            Qoute(by: doc['by'], from: doc['from'], qoute: doc['qoute']);

        return MyQouteCard(
          qoute: qoute,
          onUpdate: () {},
          onDelete: () {},
        );
      },
    ).toList();
    return myqoutes;
  }

  //Update qoute from DB
  Future updateQoute({required Qoute qoute, required String uniqueID}) async {
    final myQoute = qoute.toDatabase(qoutee: qoute);
    return await qouteCollections().doc(uniqueID.toString()).update(myQoute);
  }

  //Delete qoute from

  Future deleteQoute({required String qouteIndex}) async {
    return await qouteCollections().doc(qouteIndex).delete();
  }
}
