class Qoute {
  String by;
  String from;
  String qoute;
  int time;

  Qoute(
      {required this.by,
      required this.from,
      required this.qoute,
      this.time = 0});

  factory Qoute.fromDatabase(Map<dynamic, dynamic> data) {
    return Qoute(
      by: data['by'],
      from: data['from'],
      qoute: data['qoute'],
    );
  }

  Map<String, Object> toDatabase({required Qoute qoutee}) {
    return {
      'by': qoutee.by,
      'from': qoutee.from,
      'qoute': qoutee.qoute,
      'time': qoutee.time
    };
  }
}
