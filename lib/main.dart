import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:iqoute/custom/theme/appTheme.dart';
import 'package:iqoute/views/Welcome.dart';
import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    AppTheme appTheme = AppTheme();
    return MaterialApp(
      title: 'iQuote',
      debugShowCheckedModeBanner: false,
      theme: appTheme.themeData,
      home: const Welcome(),
    );
  }
}
